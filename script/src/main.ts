const t1 = setTimeout(() => {
    console.log('2')
}, 2000)

const t0 = setTimeout((v: any, b: any) => {
    console.log('1', v, b)
    clearTimeout(t1)
}, 1000, 'a', 'b', 12, t1);

console.log(t0, t1)

let interval: any
let i = 0
interval = setInterval(function (v: any) {
    i++
    console.log(i, v)
    if (i == 5) {
        clearInterval(interval)
    }
}, 2000, "interval")