"use strict";
var t1 = setTimeout(function () {
    console.log('2');
}, 2000);
var t0 = setTimeout(function (v, b) {
    console.log('1', v, b);
    clearTimeout(t1);
}, 1000, 'a', 'b', 12, t1);
console.log(t0, t1);
var interval;
var i = 0;
interval = setInterval(function (v) {
    i++;
    console.log(i, v);
    if (i == 5) {
        clearInterval(interval);
    }
}, 2000, "interval");
