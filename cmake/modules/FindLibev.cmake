include(FindPackageHandleStandardArgs)

set(Libev_FOUND FALSE)
set(Libev_INCLUDE_DIRS)
set(Libev_LIBRARIES)
# 查找 頭文件
find_path(Libev_INCLUDE_DIRS NAMES libev/ev.h)
if(NOT Libev_INCLUDE_DIRS)
    if(Libev_FIND_REQUIRED)
        message(FATAL_ERROR "Libev : Could not find libev/ev.h")
    else()
        message(WARNING "Libev : Could not find libev/ev.h")
    endif()
    return()
endif()


# 查找 庫文件
find_library(Libev_LIBRARIE_EV NAMES ev)
if(NOT Libev_LIBRARIE_EV)
    if(Libev_FIND_REQUIRED)
        message(FATAL_ERROR "Libev : Could not find lib libev.a")
    else()
        message(WARNING "Libev : Could not find lib libev.a")
    endif()
    return()
endif()
list(APPEND Libev_LIBRARIES
    "${Libev_LIBRARIE_EV}"
)
list(REMOVE_DUPLICATES Libev_INCLUDE_DIRS)
list(REMOVE_DUPLICATES Libev_LIBRARIES)

message(STATUS "Found Libev: ${Libev_LIBRARIES}")

# 設置 庫 信息
find_package_handle_standard_args(Libev
		DEFAULT_MSG
		Libev_INCLUDE_DIRS
        Libev_LIBRARIES
)
