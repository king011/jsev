list(APPEND target_sources
    src/main.c
    src/extras.c
    src/hashtable.c
)
file(GLOB files 
    "${CMAKE_SOURCE_DIR}/src/duk/*.c" 
)
list(APPEND target_sources ${files})