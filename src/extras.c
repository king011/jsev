#include "extras.h"
#include "duk/duk_console.h"
#include <libev/ev.h>
#define GLOBAL_JSEV_KEY "__jsev"
void jsev_duk_init(jsev_t *env);
int jsev_init(jsev_t *env)
{
    ht_setup(&env->table, sizeof(jsev_t *), sizeof(jsev_t *), 10);
    env->loop = EV_DEFAULT;
    env->ctx = duk_create_heap_default();
    duk_console_init(env->ctx, 0);
    jsev_duk_init(env);
}
void jsev_destroy(jsev_t *env)
{
    duk_destroy_heap(env->ctx);
    ev_loop_destroy(env->loop);
}
void jsev_dump_context_stdout(duk_context *ctx)
{
    duk_push_context_dump(ctx);
    fprintf(stdout, "%s\n", duk_safe_to_string(ctx, -1));
    duk_pop(ctx);
}
static void
timeout_cb(EV_P_ ev_timer *w, int revents)
{
    jsev_t *env = w->data;
    duk_context *ctx = env->ctx;
    free(w);
    duk_get_global_string(ctx, GLOBAL_JSEV_KEY);
    {
        duk_get_prop_string(ctx, -1, "callback");
        {
            duk_push_pointer(ctx, w);
            duk_get_prop(ctx, -2);
            if (duk_is_function(ctx, -1))
            {
                duk_call(ctx, 0);
            }
            else
            {
                duk_get_prop_string(ctx, -1, "args");
                duk_get_prop_string(ctx, -2, "f");
                duk_size_t n = duk_get_length(ctx, -2);
                for (size_t i = 0; i < n; i++)
                {
                    duk_get_prop_index(ctx, -2 - i, i);
                }
                duk_call(ctx, n);
                duk_pop_2(ctx);
            }
            duk_pop(ctx);
        }
        {
            duk_push_pointer(ctx, w);
            duk_del_prop(ctx, -2);
        }
    }
    duk_pop_2(ctx);
    // jsev_dump_context_stdout(ctx);
}

static void
interval_cb(EV_P_ ev_timer *w, int revents)
{
    jsev_t *env = w->data;
    duk_context *ctx = env->ctx;
    ev_timer_again(env->loop, w);
    duk_get_global_string(ctx, GLOBAL_JSEV_KEY);
    {
        duk_get_prop_string(ctx, -1, "callback");
        {
            duk_push_pointer(ctx, w);
            duk_get_prop(ctx, -2);
            if (duk_is_function(ctx, -1))
            {
                duk_call(ctx, 0);
            }
            else
            {
                duk_get_prop_string(ctx, -1, "args");
                duk_get_prop_string(ctx, -2, "f");
                duk_size_t n = duk_get_length(ctx, -2);
                for (size_t i = 0; i < n; i++)
                {
                    duk_get_prop_index(ctx, -2 - i, i);
                }
                duk_call(ctx, n);
                duk_pop_2(ctx);
            }
            duk_pop(ctx);
        }
    }
    duk_pop_2(ctx);
    // jsev_dump_context_stdout(ctx);
}

duk_ret_t jsev_native_setTimeout(duk_context *ctx)
{
    duk_idx_t n = duk_get_top(ctx);
    if (n < 1 || !duk_is_function(ctx, 0))
    {
        return 0;
    }
    ev_tstamp after = 0.0;
    if (n > 1)
    {
        after = duk_require_number(ctx, 1) / 1000;
    }
    ev_timer *timer = 0;
    duk_get_global_string(ctx, GLOBAL_JSEV_KEY);
    {
        duk_get_prop_string(ctx, -1, "env");
        jsev_t *env = duk_require_pointer(ctx, -1);
        duk_pop(ctx);

        timer = (ev_timer *)malloc(sizeof(ev_timer));
        timer->data = env;
        ev_timer_init(timer, timeout_cb, after, 0.);
        ev_timer_start(env->loop, timer);

        duk_get_prop_string(ctx, -1, "callback");
        {
            duk_push_pointer(ctx, timer);
            if (n < 3)
            {
                duk_dup(ctx, 0);
            }
            else
            {
                duk_push_object(ctx);
                duk_dup(ctx, 0);
                duk_put_prop_string(ctx, -2, "f");
                duk_push_array(ctx);
                for (size_t i = 2; i < n; i++)
                {
                    duk_dup(ctx, i);
                    duk_put_prop_index(ctx, -2, i - 2);
                }
                duk_put_prop_string(ctx, -2, "args");
            }
            duk_put_prop(ctx, -3);
        }
        duk_pop(ctx);
    }
    duk_pop(ctx);
    // jsev_dump_context_stdout(ctx);
    duk_push_pointer(ctx, timer);
    return 1;
}
duk_ret_t jsev_native_clearTimeout(duk_context *ctx)
{
    duk_idx_t n = duk_get_top(ctx);
    if (n < 1 || !duk_is_pointer(ctx, 0))
    {
        return 0;
    }

    ev_timer *timer = duk_require_pointer(ctx, 0);
    duk_get_global_string(ctx, GLOBAL_JSEV_KEY);
    {
        duk_get_prop_string(ctx, -1, "env");
        jsev_t *env = duk_require_pointer(ctx, -1);
        duk_pop(ctx);

        duk_get_prop_string(ctx, -1, "callback");
        {
            duk_push_pointer(ctx, timer);
            duk_del_prop(ctx, -2);

            ev_timer_stop(env->loop, timer);
        }
    }
    free(timer);
    duk_pop_2(ctx);
    //jsev_dump_context_stdout(ctx);
    return 0;
}
duk_ret_t jsev_native_setInterval(duk_context *ctx)
{
    duk_idx_t n = duk_get_top(ctx);
    if (n < 1 || !duk_is_function(ctx, 0))
    {
        return 0;
    }
    ev_tstamp after = 0.0;
    if (n > 1)
    {
        after = duk_require_number(ctx, 1) / 1000;
    }
    ev_timer *timer = 0;
    duk_get_global_string(ctx, GLOBAL_JSEV_KEY);
    {
        duk_get_prop_string(ctx, -1, "env");
        jsev_t *env = duk_require_pointer(ctx, -1);
        duk_pop(ctx);

        timer = (ev_timer *)malloc(sizeof(ev_timer));
        timer->data = env;
        ev_timer_init(timer, interval_cb, 0., after);
        ev_timer_again(env->loop, timer);

        duk_get_prop_string(ctx, -1, "callback");
        {
            duk_push_pointer(ctx, timer);
            if (n < 3)
            {
                duk_dup(ctx, 0);
            }
            else
            {
                duk_push_object(ctx);
                duk_dup(ctx, 0);
                duk_put_prop_string(ctx, -2, "f");
                duk_push_array(ctx);
                for (size_t i = 2; i < n; i++)
                {
                    duk_dup(ctx, i);
                    duk_put_prop_index(ctx, -2, i - 2);
                }
                duk_put_prop_string(ctx, -2, "args");
            }
            duk_put_prop(ctx, -3);
        }
        duk_pop(ctx);
    }
    duk_pop(ctx);
    // jsev_dump_context_stdout(ctx);
    duk_push_pointer(ctx, timer);
    return 1;
}

void jsev_duk_init(jsev_t *env)
{
    duk_context *ctx = env->ctx;
    duk_push_global_object(ctx);
    {
        duk_push_object(ctx);
        {
            duk_push_pointer(ctx, env);
            duk_put_prop_string(ctx, -2, "env");

            duk_push_object(ctx);
            duk_put_prop_string(ctx, -2, "callback");
        }
        duk_put_prop_string(ctx, -2, GLOBAL_JSEV_KEY);

        duk_push_c_function(ctx, jsev_native_setTimeout, DUK_VARARGS);
        duk_put_prop_string(ctx, -2, "setTimeout");

        duk_push_c_function(ctx, jsev_native_clearTimeout, 1);
        duk_put_prop_string(ctx, -2, "clearTimeout");

        duk_push_c_function(ctx, jsev_native_setInterval, DUK_VARARGS);
        duk_put_prop_string(ctx, -2, "setInterval");
        duk_push_c_function(ctx, jsev_native_clearTimeout, 1);
        duk_put_prop_string(ctx, -2, "clearInterval");
    }
    duk_pop(ctx);
    //jsev_dump_context_stdout(ctx);
}