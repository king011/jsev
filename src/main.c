#include <stdio.h>
#include <libev/ev.h>
#include "duk/duktape.h"
#include "duk/duk_console.h"
#include "extras.h"

char *readFile(const char *filename)
{
    FILE *f;
    f = fopen(filename, "r");
    if (!f)
    {
        printf("open file error : %s", filename);
        return 0;
    }
    fseek(f, 0, SEEK_END);
    int n = ftell(f);
    char *buffer = (char *)malloc(n + 1);
    buffer[n] = 0;
    fseek(f, 0, SEEK_SET);
    fread(buffer, 1, n, f);
    fclose(f);
    return buffer;
}

int main(int argc, char *argv[])
{
    char *filename;
    jsev_t env;
    jsev_init(&env);
    if (argc >= 2)
    {
        filename = argv[1];
    }
    else
    {
        filename = "main.js";
    }
    if (duk_peval_string(env.ctx, readFile(filename)))
    {
        puts(duk_to_string(env.ctx, -1));
    }
    duk_pop(env.ctx);
    ev_run(env.loop, 0);
    jsev_destroy(&env);
    return 0;
}
