#ifndef __jsev_EXTRAS_H__
#define __jsev_EXTRAS_H__
#include <libev/ev.h>
#include "duk/duktape.h"
#include "hashtable.h"
typedef struct jsev_t
{
    duk_context *ctx;
    struct ev_loop *loop;
    HashTable table;
} jsev_t;
int jsev_init(jsev_t *env);
void jsev_destroy(jsev_t *env);
#endif //__jsev_EXTRAS_H__